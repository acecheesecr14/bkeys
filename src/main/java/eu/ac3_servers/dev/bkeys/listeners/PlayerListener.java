package eu.ac3_servers.dev.bkeys.listeners;

import java.util.HashSet;
import java.util.UUID;

import eu.ac3_servers.dev.bkeys.BKeysPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerListener implements Listener{

	private BKeysPlugin plugin;
	
	public PlayerListener(BKeysPlugin plugin) {
		
		plugin.d("Registering listener: " + getClass().getName());
		plugin.getProxy().getPluginManager().registerListener(plugin, this);
		plugin.d("Registered.");
		
		this.plugin = plugin;
		
	}
	
	@EventHandler
	public void onLogin(LoginEvent e){
		
		//This removes the port from the address!
		String address = e.getConnection().getVirtualHost().getAddress().getCanonicalHostName();
		System.out.println(address);
		int port = address.indexOf(':');
        if (port != -1) {
            address = address.substring(0, port);
        }
        
        this.plugin.d(e.toString(), address);
		HashSet<String> keys = getKey(e.getConnection().getUniqueId(), e.getConnection().getName());
		
		if(keys.size() < 1) return;
		
		for (String string : keys) {
			
			if(address.equalsIgnoreCase(string)) return;
			
		}
		
		//KICK THE PLAYER.
		e.setCancelReason(ChatColor.DARK_RED + "Invalid host-key!");
		e.setCancelled(true);
		
	}

	private HashSet<String> getKey(UUID uniqueId, String name) {
		
		HashSet<String> keys = new HashSet<String>();
		
		this.plugin.d("GetKey: ", uniqueId, name);
		
		if(uniqueId != null){
			String key = this.plugin.getConfig().getString("keys.uuid." + uniqueId.toString());
			this.plugin.d("Found key: " + key);
			if(key != null)
				keys.add(key);
		}
		if(name != null){
			String key = this.plugin.getConfig().getString("keys.name." + name);
			this.plugin.d("Found key: " + key);
			if(key != null)
				keys.add(key);
		}
		
		return keys;
		
	}
	
}
