package eu.ac3_servers.dev.bkeys.commands;

import java.util.HashSet;
import java.util.UUID;

import eu.ac3_servers.dev.bkeys.BKeysPlugin;
import me.darkdeagle.bukkit.utils.TabCompletionHelper;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class AddKeyCommand extends Command implements TabExecutor {
	
	TextComponent usage = new TextComponent("Correct usage: ");
	private BKeysPlugin plugin;
	
	public AddKeyCommand(BKeysPlugin plugin) {
		
		super("addkey", "bkeys.admin", new String[0]);
		
		plugin.d("Registering command: " + getName());
		plugin.getProxy().getPluginManager().registerCommand(plugin, this);
		plugin.d("Registered.");
		
		this.plugin = plugin;
		
		//Set for usage.
		usage.setColor(ChatColor.RED);
		TextComponent usageCommand = new TextComponent("/addkey [name] [hostname] <uuid|name>");
		usageCommand.setColor(ChatColor.GREEN);
		usageCommand.setClickEvent(new ClickEvent(Action.SUGGEST_COMMAND, "/addkey"));
		usageCommand.setUnderlined(true);
		usage.addExtra(usageCommand);
		
	}

	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		HashSet<String> suggestions = new HashSet<String>();
		
		if(args.length == 0 || args.length == 1){
			return TabCompletionHelper.getPossibleCompletionsForGivenArgs(args, 
					TabCompletionHelper.getOnlinePlayerNames(this.plugin)
				);
		}else if(args.length == 2 || args.length == 3){
			return TabCompletionHelper.getPossibleCompletionsForGivenArgs(args, new String[]{ 
					"name",
					"uuid"
				});
		}else if(args.length > 3){
			suggestions.add("?");
		}
		return suggestions;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {

		if(args.length >= 2 && args.length <= 3){
			
			String playerName = args[0];
			String hostName = args[1];
			String mode = null;
			if(args.length == 3){
				mode = args[2];
			}
			
			ProxiedPlayer player = null;
			for (ProxiedPlayer p : this.plugin.getProxy().getPlayers()) {
				if(playerName.equalsIgnoreCase(p.getName())){
					player = p;
					break;
				}
			}
			
			if(player == null){
				sender.sendMessage(ChatColor.RED + "That player doesn't exist, or isn't online!");
				usage(sender);
				return;
			}
			
			if(mode != null && mode.equalsIgnoreCase("uuid")){
				
				UUID playerUUID = player.getUniqueId();
				
				this.plugin.getConfig().set("keys.uuid" + playerUUID.toString(), hostName);
				
				this.plugin.saveConfig();
				
				sender.sendMessage(ChatColor.BLUE + "Added key for " + playerName + " as UUID only.");
				
			}else if(mode != null && mode.equalsIgnoreCase("name")){
				
				this.plugin.getConfig().set("keys.name" + playerName, hostName);
				
				this.plugin.saveConfig();
				
				sender.sendMessage(ChatColor.BLUE + "Added key for " + playerName + " as name only.");
				
			}else{
				
				UUID playerUUID = player.getUniqueId();
				
				this.plugin.getConfig().set("keys.uuid" + playerUUID.toString(), hostName);
				this.plugin.getConfig().set("keys.name" + playerName, hostName);
				
				this.plugin.saveConfig();
				
				sender.sendMessage(ChatColor.BLUE + "Added key for " + playerName + " as UUID and name.");
				
			}
				
		}else{
			
			usage(sender);
			
		}

	}

	private void usage(CommandSender sender) {
		
		sender.sendMessage(usage);
		
	}

}
