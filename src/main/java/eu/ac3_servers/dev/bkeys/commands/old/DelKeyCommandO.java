package eu.ac3_servers.dev.bkeys.commands.old;

import java.util.HashSet;

import eu.ac3_servers.dev.bkeys.BKeysPlugin;
import me.darkdeagle.bukkit.utils.TabCompletionHelper;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class DelKeyCommandO extends Command implements TabExecutor {
	
	TextComponent usage = new TextComponent("Correct usage: ");
	private BKeysPlugin plugin;
	
	public DelKeyCommandO(BKeysPlugin plugin) {
		
		super("delkey", "bkeys.admin", new String[0]);
		
		plugin.d("Registering command: " + getName() + " OLD");
		plugin.getProxy().getPluginManager().registerCommand(plugin, this);
		plugin.d("Registered.");
		
		this.plugin = plugin;
		
		//Set for usage.
		usage.setColor(ChatColor.RED);
		TextComponent usageCommand = new TextComponent("/delkey [name]");
		usageCommand.setColor(ChatColor.GREEN);
		usageCommand.setClickEvent(new ClickEvent(Action.SUGGEST_COMMAND, "/delkey"));
		usageCommand.setUnderlined(true);
		
	}

	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		HashSet<String> suggestions = new HashSet<String>();
		
		if(args.length == 0 || args.length == 1){
			return TabCompletionHelper.getPossibleCompletionsForGivenArgs(args, 
					TabCompletionHelper.getOnlinePlayerNames(this.plugin)
				);
		}else if(args.length > 1){
			suggestions.add("?");
		}
		return suggestions;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {

		if(args.length == 1){
			
			String playerName = args[0];
			
			ProxiedPlayer player = null;
			for (ProxiedPlayer p : this.plugin.getProxy().getPlayers()) {
				if(playerName.equalsIgnoreCase(p.getName())){
					player = p;
					break;
				}
			}
			
			if(player == null){
				sender.sendMessage(ChatColor.RED + "That player doesn't exist, or isn't online!");
				usage(sender);
				return;
			}
			
			this.plugin.getConfig().set("keys.name" + playerName, null);
			this.plugin.saveConfig();
			sender.sendMessage(ChatColor.BLUE + "deleted keys for " + playerName);
				
		}else{
			
			usage(sender);
			
		}

	}

	private void usage(CommandSender sender) {
		
		sender.sendMessage(usage);
		
	}

}
