package eu.ac3_servers.dev.bkeys.commands.old;

import java.util.HashSet;

import eu.ac3_servers.dev.bkeys.BKeysPlugin;
import me.darkdeagle.bukkit.utils.TabCompletionHelper;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class AddKeyCommandO extends Command implements TabExecutor {
	
	String usage = ChatColor.RED + "Correct usage: " + ChatColor.GREEN + "/addkey [name] [hostname]";
	private BKeysPlugin plugin;
	
	public AddKeyCommandO(BKeysPlugin plugin) {
		
		super("addkey", "bkeys.admin", new String[0]);
	
		plugin.d("Registering command: " + getName() + " OLD");
		plugin.getProxy().getPluginManager().registerCommand(plugin, this);
		plugin.d("Registered.");
		
		this.plugin = plugin;
		
	}

	@Override
	public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
		HashSet<String> suggestions = new HashSet<String>();
		
		if(args.length == 0 || args.length == 1){
			return TabCompletionHelper.getPossibleCompletionsForGivenArgs(args, 
					TabCompletionHelper.getOnlinePlayerNames(this.plugin)
				);
		}else if(args.length > 2){
			suggestions.add("?");
		}
		return suggestions;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {

		if(args.length == 2){
			
			String playerName = args[0];
			String hostName = args[1];
			
			ProxiedPlayer player = null;
			for (ProxiedPlayer p : this.plugin.getProxy().getPlayers()) {
				if(playerName.equalsIgnoreCase(p.getName())){
					player = p;
					break;
				}
			}
			
			if(player == null){
				sender.sendMessage(ChatColor.RED + "That player doesn't exist, or isn't online!");
				usage(sender);
				return;
			}
			
			this.plugin.getConfig().set("keys.name" + playerName, hostName);
			this.plugin.saveConfig();
			sender.sendMessage(ChatColor.BLUE + "Added key for " + playerName);	
				
		}else{
			
			usage(sender);
			
		}

	}

	@SuppressWarnings("deprecation")
	private void usage(CommandSender sender) {
		
		sender.sendMessage(usage);
		
	}

}
