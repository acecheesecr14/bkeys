package eu.ac3_servers.dev.bkeys.commands.old;

import eu.ac3_servers.dev.bkeys.BKeysPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class BKeysCommandO extends Command {

	private BKeysPlugin plugin;

	String debugCommand = ChatColor.GREEN + "Toggle debug - " + ChatColor.BLUE + "/BKeys debug";
	String addKeyCommand = ChatColor.GREEN + "Add a key    - " + ChatColor.BLUE + "/addkey";
	String delKeyCommand = ChatColor.GREEN + "Remove a key - " + ChatColor.BLUE + "/delkey";
		
	public BKeysCommandO(BKeysPlugin plugin) {
		super("BKeys", null, new String[]{"bkey", "bk"});
		
		plugin.d("Registering command: " + getName() + " OLD.");
		plugin.getProxy().getPluginManager().registerCommand(plugin, this);
		plugin.d("Registered.");
		
		this.plugin = plugin;
		
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length == 1 && args[0].equalsIgnoreCase("debug")){
			
			if(this.plugin.isDebug()){
				this.plugin.getConfig().set("debugging", false);
			}else{
				this.plugin.getConfig().set("debugging", true);
			}
			this.plugin.saveConfig();
			String saved = ChatColor.GREEN + "Debugging is" + ChatColor.RED + this.plugin.isDebug();
			sender.sendMessage(saved);

			this.plugin.d("Debugging turned on.");
			return;
			
		}
		
		String tc0 = "BKeys v" + this.plugin.getDescription().getVersion();
		
		sender.sendMessage(tc0);
		
		if(sender.hasPermission("bkeys.admin")){

			sender.sendMessage(debugCommand);
			sender.sendMessage(addKeyCommand);
			sender.sendMessage(delKeyCommand);

		}
		
	}

}
