package eu.ac3_servers.dev.bkeys.commands;

import eu.ac3_servers.dev.bkeys.BKeysPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

public class BKeysCommand extends Command {

	private BKeysPlugin plugin;

	TextComponent debugCommand = new TextComponent("Toggle debug - ");
	TextComponent addKeyCommand = new TextComponent("Add a key    - ");
	TextComponent delKeyCommand = new TextComponent("Remove a key - ");
		
	public BKeysCommand(BKeysPlugin plugin) {
		super("BKeys", null, new String[]{"bkey", "bk"});
		
		plugin.d("Registering command: " + getName());
		plugin.getProxy().getPluginManager().registerCommand(plugin, this);
		plugin.d("Registered.");
		
		this.plugin = plugin;
		
		//Set for debugCommand.
		debugCommand.setColor(ChatColor.GREEN);
		TextComponent debugCommand0 = new TextComponent("/BKeys debug");
		debugCommand0.setClickEvent(new ClickEvent(Action.SUGGEST_COMMAND, "/BKeys debug"));
		debugCommand0.setUnderlined(true);
		debugCommand0.setColor(ChatColor.LIGHT_PURPLE);
		debugCommand.addExtra(debugCommand0);
		
		//Set for addKeyCommand.
		addKeyCommand.setColor(ChatColor.GREEN);
		TextComponent addKeyCommand0 = new TextComponent("/addkey");
		addKeyCommand0.setClickEvent(new ClickEvent(Action.SUGGEST_COMMAND, "/addkey "));
		addKeyCommand0.setUnderlined(true);
		addKeyCommand0.setColor(ChatColor.LIGHT_PURPLE);
		addKeyCommand.addExtra(addKeyCommand0);
		
		//Set for delKeyCommand.
		delKeyCommand.setColor(ChatColor.GREEN);
		TextComponent delKeyCommand0 = new TextComponent("/delkey");
		delKeyCommand0.setClickEvent(new ClickEvent(Action.SUGGEST_COMMAND, "/delkey "));
		delKeyCommand0.setUnderlined(true);
		delKeyCommand0.setColor(ChatColor.LIGHT_PURPLE);
		delKeyCommand.addExtra(delKeyCommand0);
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length == 1 && args[0].equalsIgnoreCase("debug")){
			
			if(this.plugin.isDebug()){
				this.plugin.getConfig().set("debugging", false);
			}else{
				this.plugin.getConfig().set("debugging", true);
			}
			this.plugin.saveConfig();
			TextComponent saved = new TextComponent("Debugging is");
			saved.setColor(ChatColor.GREEN);
			
			TextComponent state = new TextComponent(" " + this.plugin.isDebug());
			state.setColor(ChatColor.RED);
			
			saved.addExtra(state);
			sender.sendMessage(saved);
			this.plugin.d("Debugging turned on.");
			return;
			
		}
		
		TextComponent tc0 = new TextComponent("BKeys v" + this.plugin.getDescription().getVersion());
		tc0.setColor(ChatColor.GREEN);
		
		sender.sendMessage(tc0);
		
		if(sender.hasPermission("bkeys.admin")){

			sender.sendMessage(debugCommand);
			sender.sendMessage(addKeyCommand);
			sender.sendMessage(delKeyCommand);

		}
		
	}

}
