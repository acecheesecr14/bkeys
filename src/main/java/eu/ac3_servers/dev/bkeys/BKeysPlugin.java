package eu.ac3_servers.dev.bkeys;

import eu.ac3_servers.dev.bkeys.commands.AddKeyCommand;
import eu.ac3_servers.dev.bkeys.commands.BKeysCommand;
import eu.ac3_servers.dev.bkeys.commands.DelKeyCommand;
import eu.ac3_servers.dev.bkeys.commands.old.AddKeyCommandO;
import eu.ac3_servers.dev.bkeys.commands.old.BKeysCommandO;
import eu.ac3_servers.dev.bkeys.commands.old.DelKeyCommandO;
import eu.ac3_servers.dev.bkeys.listeners.PlayerListener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;

/**
 * This is BKeys.
 * Same concept as the host keys from WorldGuard.
 * 
 * @author Cory Redmond
 *
 */
public class BKeysPlugin extends Plugin {
	
	private BKeysConfiguration config;
	
	@Override
	public void onEnable() {
		
		this.config = new BKeysConfiguration(this);
		new PlayerListener(this);
		
		if(is6()){
			
			getLogger().warning("Old bungeecord is enabled. The messages will be alot simpler!");
			d("Registering OLD commands.");
			new BKeysCommandO(this);
			new AddKeyCommandO(this);
			new DelKeyCommandO(this);
			
		}else{
			
			d("Registering commands.");
			new BKeysCommand(this);
			new AddKeyCommand(this);
			new DelKeyCommand(this);
			
		}
		
	}
	
	@Override
	public void onDisable() {
		
		getLogger().info("I don't think I have to use the onDisable function in this plugin.");
		
	}
	
	public void d(Object... strings){
		if(isDebug()){
			
			for (Object string : strings) {
				System.out.println("[BKD] " + string);
			}
			
		}
	}
	
	public boolean isDebug(){
		return getConfig().getBoolean("debugging");
	}
	
	public Configuration getConfig(){
		return this.config.getConfig();
	}
	
	public void saveConfig(){
		this.config.saveConfig();
	}

	public boolean is6() {
		return getConfig().getBoolean("old_bungee");
	}

}
