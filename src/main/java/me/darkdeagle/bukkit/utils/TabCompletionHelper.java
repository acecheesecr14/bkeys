
package me.darkdeagle.bukkit.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import eu.ac3_servers.dev.bkeys.BKeysPlugin;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Class to help with the TabCompletion for Bukkit.
 * Can be used by anybody, as long as you provide credit for it.
 * 
 * @author D4rKDeagle
 * @author Cory Redmond
 */
public class TabCompletionHelper {

	public static List<String> getPossibleCompletionsForGivenArgs(
			String[] args, String[] possibilitiesOfCompletion) {
		String argumentToFindCompletionFor = null;
		if(args.length > 0)
			argumentToFindCompletionFor = args[args.length - 1];

		List<String> listOfPossibleCompletions = new ArrayList<String>();

		for (int i = 0; i < possibilitiesOfCompletion.length; ++i) {
			String foundString = possibilitiesOfCompletion[i];

			if (foundString.regionMatches(true, 0, argumentToFindCompletionFor,
					0, argumentToFindCompletionFor.length())) {
				listOfPossibleCompletions.add(foundString);
			}
		}

		return listOfPossibleCompletions;
	}

	public static String[] getOnlinePlayerNames(BKeysPlugin plugin) {
		Collection<ProxiedPlayer> onlinePlayers = plugin.getProxy().getPlayers();
		HashSet<String> onlinePlayerNames = new HashSet<String>();

		for (ProxiedPlayer player : onlinePlayers) {
			onlinePlayerNames.add(player.getName());
		}

		return onlinePlayerNames.toArray(new String[onlinePlayerNames.size()]);
	}
}